package ants;

import core.Ant;
import core.AntColony;

/**
 * An Ant that protects an other ant
 *
 * @author Farchetto Xavier, Sadik Nassere 
 */
public class BodyguardAnt extends Ant implements Containing{

	/**
	 * Creates a new Bodyguard Ant
	 * Armor: 2, Food:5, Damage: 0
	 */	
	
	private Ant protect; //this ant is the one protected by the bodyguard ant
	
	public BodyguardAnt () {
		super(2,5,true);
		damage = 0;
	}

	public void action (AntColony colony) {
		//no action because it just protect an other ant
	}
	
	public boolean addContenantInsect(Ant ant){
		this.protect = ant;
		return this.protect == ant;
	}
	
	public Ant getContenantAnt(){
		return this.protect;
	}
}