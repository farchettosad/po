package ants;
import core.Ant;

/**
 * Interface which provides the duality of ants at the same place
 * 
 * @author Farchetto Xavier, Sadik Nassere 
 */
public interface Containing {
	
	public boolean addContenantInsect(Ant ant);
	public Ant getContenantAnt();

}
