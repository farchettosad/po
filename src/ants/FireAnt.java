package ants;

import core.Ant;
import core.AntColony;
import core.Bee;

/**the ant who burn all the bees present in the tunnel
 * 
 * 
 * @author Farchetto Xavier, Sadik Nassere 
 */

public class FireAnt extends Ant {

	/**
	 * Creates a new Fire Ant
	 * Armor: 1, Food: 4, Damage: bee's life
	 */
	public FireAnt () {
		super(1,4,true);
		damage = 3;
	}
	

	public Bee[] getTargetBee () {
			Bee[] bee = place.getBees();
			return bee;
	}
	
	public void reduceArmor (int amount) {
		armor -= amount;
		
		if (armor <= 0) {
			Bee[] bees = getTargetBee();
			
			for (int i = 0; i<bees.length; i++){
				bees[i].reduceArmor(this.getResultDamage());
			}
			leavePlace();
		
		}
	}
	


	public void action (AntColony colony) {
		//No action for the fireAnt
	}
}