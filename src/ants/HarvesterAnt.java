package ants;

import core.Ant;
import core.AntColony;

/**
 * An Ant that harvests food
 *
 * @author Farchetto Xavier. Sadik Nassere
 */
public class HarvesterAnt extends Ant {

	/**
	 * Creates a new Harvester Ant
	 * Armor: 1, Food: 2, Damage: 0
	 */
	public HarvesterAnt () {
		super(1,2,true);
		damage = 0;
	}

	
	public void action (AntColony colony) {
			colony.increaseFood(1);
	}
}