package ants;

import core.Ant;
import core.AntColony;
import core.Bee;

/**
 * An Ant that harvests food
 *
 * @author Farchetto Xavier, Sadik Nassere 
 */
public class HungryAnt extends Ant {

	int roundMax=3;
	/**
	 * Creates a new Hungry Ant
	 * Armor: 1, Food: 4, Damage: bee's life with a break of 3 steps
	 */
	public HungryAnt () {
		super(1,4,true);
		damage = 0;
	}

	public Bee getTarget () {
		return place.getClosestBee(0,0);
	}

	
	public void action (AntColony colony) {
	if(roundMax!=3){
		roundMax++;
	}else{
		Bee target = getTarget();
		if (target != null) {
			target.reduceArmor(target.getArmor());
			roundMax=0;
		}
		}
	}
}
