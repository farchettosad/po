package ants;

import core.Ant;
import core.AntColony;
import core.Bee;

/**
 * An ant who throws leaves at bees bigger than ThowerAnt
 *
 * @author Farchetto Xavier, Sadik Nassere
 */
public class LongThrowerAnt extends ThrowerAnt {

	/**
	 * Creates a new Thrower Ant.
	 * Armor: 1, Food: 3, Damage: 1
	 */
	public LongThrowerAnt () {
		this.foodCost = 3;
		this.minThrow = 4;
		this.maxThrow = 20;
	}
}