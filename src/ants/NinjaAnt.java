package ants;

import core.Ant;
import core.AntColony;
import core.Bee;

/**An inivisible ant whick attacks all the bees
 *  
 * @author Farchetto Xavier, Sadik Nassere 
 *
 */
public class NinjaAnt extends Ant {

	
	/**
	 * Creates a new Ninja Ant
	 * Armor: 1, Food: 6, Damage: 1
	 */
	public NinjaAnt () {
		super(1,6,false);
		damage = 1;
	}
	
	public Bee[] getTargetBee () {
			Bee[] bee = place.getBees();
			return bee;
	}


	public void action (AntColony colony) {
		Bee[] bees = getTargetBee();
		for (int i = 0; i<bees.length; i++){
			bees[i].reduceArmor(this.getResultDamage());
		}
	}
}