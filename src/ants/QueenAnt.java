package ants;

import core.AntColony;
import core.Bee;
import core.ScubaAnt;

/**The masterpiece of the game
 *  
 * @author Farchetto Xavier, Sadik Nassere 
 *
 */
public class QueenAnt extends ScubaAnt {
	
	/**Create the QueenAnt
	 * Armor: 1, Food: 5, Damage: 1
	 * 
	 * */
	
	public QueenAnt () {
		super(1,6,true);
		damage = 0;
	}

	public Bee getTarget () {
		return place.getClosestBee(0, 3);
	}


	public void action (AntColony colony) {
		if (place.getEntrance().getAnt() != null){
			
		} else if (place.getExit().getAnt() != null){
			
		}
	}
}