package ants;

import core.AntColony;
import core.Bee;
import core.ScubaAnt;

/**
 * Same ant as the throwerAnt but it lives only in water
 *
 * @author Farchetto Xavier, Sadik Nassere
 */
public class ScubaThrowerAnt extends ScubaAnt {

	/**
	 * Creates a new ScubaThrower Ant.
	 * Armor: 1, Food: 5, Damage: 1
	 */
	public ScubaThrowerAnt () {
		super(1,5,true);
		damage = 1;
	}

	/**
	 * Returns a target for this ant
	 *
	 * @return A bee to target
	 */
	public Bee getTarget () {
		return place.getClosestBee(0, 3);
	}

	
	public void action (AntColony colony) {
		Bee target = getTarget();
		if (target != null) {
			target.reduceArmor(this.getResultDamage());
		}
	}
}