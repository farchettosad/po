package ants;

import core.Ant;
import core.AntColony;
import core.Bee;

/**
 * An ant who throws leaves at bees lower than ThowerAnt
 *
 * @author Farchetto Xavier, Sadik Nassere
 */
public class ShortThrowerAnt extends ThrowerAnt {

	/**
	 * Creates a new Thrower Ant.
	 * Armor: 1, Food: 3, Damage: 1
	 */
	public ShortThrowerAnt () {
		this.foodCost = 3;
		this.minThrow = 0;
		this.maxThrow = 2;
	}
}