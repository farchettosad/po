package ants;

import core.Ant;
import core.AntColony;

/**
 * An Ant that harvests food
 *
 * @author Farchetto Xavier,Sadik Nassere
 */
public class WallAnt extends Ant {

	/**
	 * Creates a new Harvester Ant
	 * Armor: 4, Food: 4, Damage: 0
	 */
	public WallAnt () {
		super(4,4,true);
		
		damage = 0;
	}

	@Override
	public void action (AntColony colony) {
		//No action for the wallAnt
	}
}
