package core;

/**
 * A class representing a basic Ant
 *
 * @author Farchetto Xavier, Sadik Nassere
 */
public abstract class Ant extends Insect {

	protected int foodCost; // the amount of food needed to make this ant
	protected boolean visible;
	protected int damage;
	
	/**
	 * Creates a new Ant, with a food cost of 0.
	 *
	 * @param armor
	 *            The armor of the ant.
	 */
	public Ant (int armor, int foodCost, boolean visible) {
		super(armor, null);
		this.armor = armor;
		this.foodCost = foodCost;
	}
	
	/**
	 * Returns the ant's food cost
	 *
	 * @return the ant's good cost
	 */
	public int getFoodCost () {
		return foodCost;
	}
	
	
	/**
	 * Removes the ant from its current place
	 */
	
	public void leavePlace () {
		place.removeInsect(this);
	}

	// A great power implies huge responsibilities : Double the power of all the ants
	public int getResultDamage() {
		if (this.getPlace().getEntrance() != null) {
			if (this.getPlace().getEntrance().getAnt() instanceof ants.QueenAnt){
				return damage*2;
			}
		}
		if (this.getPlace().getExit() != null) {
			if (this.getPlace().getExit().getAnt() instanceof ants.QueenAnt){
				return damage*2;
			}
		}
		return damage;
		
	}
	
}