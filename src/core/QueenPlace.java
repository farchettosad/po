package core;

/**
* @author Farchetto Xavier. Sadik Nassere
*/

public class QueenPlace extends Place {
	
	//Where the queen is currently 
	private Place realQueenPlace; 

	public QueenPlace(String name) {
		super(name, null);
	}

	public boolean addContenantQueen(Place place){
		this.realQueenPlace = place;
		return this.realQueenPlace == place;
	}
	
	public Place getContenantPlace(){
		return this.realQueenPlace;
	}

	public boolean removeContenantPlace() {
		this.realQueenPlace = null;
		return this.realQueenPlace == null;
	}
	
	
}
