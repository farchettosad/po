package core;

/**
* @author Farchetto Xavier. Sadik Nassere
*/

//Plays the role of a scubaThrowerAnt or a Queen

public class ScubaAnt extends Ant {
	
	protected boolean watersafe;
	
	
	public ScubaAnt(int armor, int foodCost, boolean visible) {
		super(armor, foodCost, visible);
		watersafe=true;
	}

	public void action(AntColony colony) {

	}

}
